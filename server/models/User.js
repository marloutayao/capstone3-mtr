const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
	{
		username: { type: String, required: true, unique: true },
		password: { type: String, required: true },
		email: { type: String },
		firstName: { type: String },
		lastName: { type: String },
		contact_number: { type: String },
		role: { type: String, default: "user" }
	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("User", userSchema);
