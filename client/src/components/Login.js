import React, { useState } from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { loginUserMutation } from "../queries/mutations";
import { Redirect } from "react-router-dom";

const showAlert = (title, text, icon) => {
	Swal.fire({
		title: title,
		text: text,
		icon: icon
	});
};

const Login = props => {
	console.log(props);
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [isLogIn, setIsLogIn] = useState(false);
	const inputsHandler = event => {
		if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "username") {
			setUsername(event.target.value);
		}
	};

	const submitLoginForm = event => {
		event.preventDefault();
		props
			.loginUser({
				variables: {
					username: username,
					password: password
				}
			})
			.then(res => {
				if (!res.data.loginUser) {
					showAlert(
						"Error",
						"Log in failed, please try again.....",
						"error"
					);
				} else {
					Swal.fire({
						title: "Success",
						text: "you are now log in!",
						icon: "success",
						showCancelButton: false,
						confirmButtonColor: "#3085d6"
					}).then(result => {
						if (result.value) {
							localStorage.setItem(
								"username",
								res.data.loginUser.username
							);
							localStorage.setItem(
								"role",
								res.data.loginUser.role
							);
							localStorage.setItem(
								"userId",
								res.data.loginUser.id
							);

							setIsLogIn(true);
							props.updateSession();
						}
					});
				}
			});
	};

	if (isLogIn) {
		return <Redirect to="/" />;
	}

	return (
		<div className="container">
			<hr />
			<div className="row">
				<div className="col-lg-6 mx-auto col-12 col-sm-12">
					<div className="card">
						<div className="card-header text-center">
							<h3>Login</h3>
						</div>
						<div className="card-body shadow">
							<form onSubmit={submitLoginForm}>
								<div className="form-group">
									<label htmlFor="username">Username</label>
									<input
										id="username"
										type="text"
										className="form-control"
										placeholder="Username"
										onChange={inputsHandler}
										value={username}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input
										id="password"
										type="password"
										className="form-control"
										placeholder="Password"
										onChange={inputsHandler}
										value={password}
									/>
								</div>

								<button
									type="submit"
									className="btn btn-primary btn-block"
								>
									Log in
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default graphql(loginUserMutation, { name: "loginUser" })(Login);
