import React from "react";
import { Link } from "react-router-dom";

const NavBar = props => {
	let customLink = "";

	if (!props.username) {
		customLink = (
			<ul className="navbar-nav">
				<li className="nav-item">
					<Link to="/register" className="nav-link">
						Register
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/login" className="nav-link">
						Login
					</Link>
				</li>
			</ul>
		);
	} else {
		customLink = (
			<ul className="navbar-nav">
				{localStorage.getItem("role") === "user" ? (
					<li className="nav-item">
						<Link
							to={
								"/reservations/" +
								localStorage.getItem("userId")
							}
							className="nav-link"
						>
							My Reservation
						</Link>
					</li>
				) : (
					""
				)}
				<li className="nav-item">
					<Link
						to={"/users/" + localStorage.getItem("userId")}
						className="nav-link"
					>
						Profile
					</Link>
				</li>
				<li className="nav-item">
					<Link to="/logout" className="nav-link">
						Logout
					</Link>
				</li>
			</ul>
		);
	}
	return (
		<nav className="navbar navbar-expand-md bg-dark navbar-dark">
			<Link to="/" className="navbar-brand">
				MTReservations
			</Link>

			<button
				className="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#collapsibleNavbar"
			>
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="collapsibleNavbar">
				{localStorage.getItem("role") === "admin" ? (
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link to="/transactions" className="nav-link">
								Transactions
							</Link>
						</li>
						<li className="nav-item">
							<Link to="/carousel" className="nav-link">
								Carousel Images
							</Link>
						</li>
					</ul>
				) : (
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link to="/reservations" className="nav-link">
								Reserve Now!
							</Link>
						</li>
					</ul>
				)}

				{customLink}
			</div>
		</nav>
	);
};

export default NavBar;
