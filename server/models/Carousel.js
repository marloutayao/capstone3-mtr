const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Carousel = new Schema(
	{
		original: { type: String },
		thumbnail: { type: String }
	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Carousel", Carousel);
