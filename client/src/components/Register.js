import React, { useState } from "react";
import { createUserMutation } from "../queries/mutations.js";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";

const showAlert = (title, text, icon) => {
	Swal.fire({
		title: title,
		text: text,
		icon: icon
	});
};
const Register = props => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [email, setEmail] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [contact_number, setContactNumber] = useState("");

	const inputHandler = event => {
		if (event.target.id === "username") {
			setUsername(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "email") {
			setEmail(event.target.value);
		} else if (event.target.id === "firstName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lastName") {
			setLastName(event.target.value);
		} else if (event.target.id === "contact_number") {
			setContactNumber(event.target.value);
		}
	};

	const addUser = event => {
		event.preventDefault();

		if (
			username === "" ||
			password === "" ||
			email === "" ||
			firstName === "" ||
			lastName === "" ||
			contact_number === ""
		) {
			showAlert(
				"Ooops, there is an error",
				"Please complete the inputs",
				"warning"
			);
		} else {
			let newUser = {
				username: username,
				password: password,
				email: email,
				firstName: firstName,
				lastName: lastName,
				contact_number: contact_number
			};

			props
				.createUser({
					variables: newUser
				})
				.then(res => {
					showAlert(
						"Success",
						"User create, you may now log in!",
						"success"
					);
					setUsername("");
					setPassword("");
					setEmail("");
					setFirstName("");
					setLastName("");
					setContactNumber("");
				})
				.catch(error => {
					showAlert("Error", "Username is already taken!", "error");
				});
		}
	};

	return (
		<div className="container">
			<hr />
			<div className="row">
				<div className="col-lg-8 mx-auto col-12 col-sm-12">
					<div className="card">
						<div className="card-header text-center">
							<h3>Register</h3>
						</div>
						<div className="card-body shadow">
							<form onSubmit={addUser}>
								<div className="form-group">
									<label htmlFor="username">Username</label>
									<input
										id="username"
										type="text"
										className="form-control"
										placeholder="Username"
										onChange={inputHandler}
										value={username}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input
										id="password"
										type="password"
										className="form-control"
										placeholder="Password"
										onChange={inputHandler}
										value={password}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="email">Email address</label>
									<input
										id="email"
										type="email"
										className="form-control"
										placeholder="email@email.com"
										onChange={inputHandler}
										value={email}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="firstName">
										First Name
									</label>
									<input
										id="firstName"
										type="text"
										className="form-control"
										placeholder="First Name"
										onChange={inputHandler}
										value={firstName}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="lastName">Last Name</label>
									<input
										id="lastName"
										type="text"
										className="form-control"
										placeholder="Last Name"
										onChange={inputHandler}
										value={lastName}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="contact_number">
										Contact Number
									</label>
									<input
										id="contact_number"
										type="text"
										className="form-control"
										placeholder="Contact Number"
										onChange={inputHandler}
										value={contact_number}
									/>
								</div>
								<button
									type="submit"
									className="btn btn-primary btn-block"
								>
									Sign Up
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default graphql(createUserMutation, { name: "createUser" })(Register);
