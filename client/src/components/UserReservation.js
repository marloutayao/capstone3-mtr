import React, { useState } from "react";
import { getReservationsQuery } from "../queries/queries";
import { updateTransactionMutation } from "../queries/mutations";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import moment from "moment";
import Swal from "sweetalert2";

const UserReservation = props => {
	const [filterTransactions, setFilterTransactions] = useState("pending");
	const [badge_color, setBadge_color] = useState("warning");
	const transactions = props.getReservations.getTransactions
		? props.getReservations.getTransactions
		: [];

	const decline_transaction = event => {
		let transaction_id = event.target.id;
		Swal.fire({
			title: "Are you sure,",
			text: "to decline this reservation?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes"
		}).then(result => {
			if (result.value) {
				props.updateTransaction({
					variables: {
						id: transaction_id,
						status: "canceled"
					},
					refetchQueries: [
						{
							query: getReservationsQuery
						}
					]
				});

				Swal.fire("Reservation has been Declined!", "", "info");
			}
		});
	};

	const filterHandler = event => {
		if (event.target.id === "pending") {
			setFilterTransactions("pending");
			setBadge_color("warning");
		} else if (event.target.id === "approved") {
			setFilterTransactions("approved");
			setBadge_color("success");
		} else if (event.target.id === "settled") {
			setFilterTransactions("settled");
			setBadge_color("info");
		} else if (event.target.id === "canceled") {
			setFilterTransactions("canceled");
			setBadge_color("danger");
		}
	};
	return (
		<div className="container">
			<h1> My Reservations </h1>
			<div className="btn-group flex-wrap">
				<button
					onClick={filterHandler}
					id="pending"
					className="btn btn-warning"
				>
					Pending Reservation
				</button>
				<button
					onClick={filterHandler}
					id="approved"
					className="btn btn-success"
				>
					Approved Reservation
				</button>
				<button
					onClick={filterHandler}
					id="settled"
					className="btn btn-info"
				>
					Settled Reservation
				</button>
				<button
					onClick={filterHandler}
					id="canceled"
					className="btn btn-danger"
				>
					Canceled Reservation
				</button>
			</div>
			<hr />
			<div className="row">
				<div className="col-lg-12">
					<table className="table table-striped">
						<thead>
							<tr>
								<th>Check in</th>
								<th>Check out</th>
								<th>Status</th>
								{filterTransactions === "pending" ? (
									<th> Actions </th>
								) : (
									""
								)}
							</tr>
						</thead>
						<tbody>
							{transactions.map(transaction => {
								let check_in = moment(transaction.check_in)
									.subtract(1, "d")
									.format("MMMM Do, YYYY");
								let check_out = moment(transaction.check_out)
									.subtract(1, "d")
									.format("MMMM Do, YYYY");
								if (
									transaction.status === filterTransactions &&
									transaction.userId ===
										localStorage.getItem("userId")
								) {
									return (
										<tr key={transaction.id}>
											<td>{check_in}</td>
											<td>{check_out}</td>
											<td>
												<span
													className={
														"badge badge-" +
														badge_color
													}
												>
													{transaction.status.toUpperCase()}
												</span>
											</td>
											{transaction.status ===
											"pending" ? (
												<td>
													<button
														onClick={
															decline_transaction
														}
														id={transaction.id}
														className="btn btn-outline-danger"
													>
														Cancel
													</button>
												</td>
											) : (
												""
											)}
										</tr>
									);
								}
							})}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default compose(
	graphql(getReservationsQuery, { name: "getReservations" }),
	graphql(updateTransactionMutation, { name: "updateTransaction" })
)(UserReservation);
