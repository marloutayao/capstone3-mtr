import { gql } from "apollo-boost";

const createUserMutation = gql`
	mutation(
		$username: String!
		$password: String!
		$email: String!
		$firstName: String!
		$lastName: String!
		$contact_number: String!
	) {
		createUser(
			username: $username
			password: $password
			email: $email
			firstName: $firstName
			lastName: $lastName
			contact_number: $contact_number
		) {
			id
			username
			password
			email
			firstName
			lastName
			contact_number
		}
	}
`;

const loginUserMutation = gql`
	mutation($username: String!, $password: String!) {
		loginUser(username: $username, password: $password) {
			id
			username
			email
			firstName
			lastName
			contact_number
			role
		}
	}
`;

const createReservationMutation = gql`
	mutation($userId: String!, $check_in: Date!, $check_out: Date!) {
		createTransaction(
			userId: $userId
			check_in: $check_in
			check_out: $check_out
		) {
			id
			userId
			check_in
			check_out
			status
		}
	}
`;

const updateTransactionMutation = gql`
	mutation($id: ID!, $status: String!) {
		updateTransaction(id: $id, status: $status)
	}
`;

const updateUserMutation = gql`
	mutation(
		$id: ID!
		$password: String
		$email: String
		$firstName: String
		$lastName: String
		$contact_number: String
	) {
		updateUser(
			id: $id
			password: $password
			email: $email
			firstName: $firstName
			lastName: $lastName
			contact_number: $contact_number
		) {
			id
			username
			password
			email
			firstName
			lastName
			contact_number
		}
	}
`;

const addCarouselImageMutation = gql`
	mutation($original: String!) {
		addCarouselImage(original: $original) {
			original
			thumbnail
		}
	}
`;

const deleteCarouselImageMutation = gql`
	mutation($id: ID!) {
		deleteCarouselImage(id: $id)
	}
`;
export {
	createUserMutation,
	loginUserMutation,
	createReservationMutation,
	updateTransactionMutation,
	updateUserMutation,
	addCarouselImageMutation,
	deleteCarouselImageMutation
};
