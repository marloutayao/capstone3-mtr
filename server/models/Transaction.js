const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema(
	{
		userId: { type: String },
		check_in: { type: Date },
		check_out: { type: Date },
		status: { type: String, default: "pending" }
	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Transaction", transactionSchema);
