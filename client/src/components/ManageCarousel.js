import React, { useState } from "react";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { toBase64, nodeServer } from "../function.js";
import Swal from "sweetalert2";

import {
	addCarouselImageMutation,
	deleteCarouselImageMutation
} from "../queries/mutations";
import { getCarouselImagesQuery } from "../queries/queries";

const ManageCarousel = props => {
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	const imagePathHandler = event => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setImagePath(encodedFile);
		});
	};

	const carouselImages = props.getCarouselImages.getCarousels
		? props.getCarouselImages.getCarousels
		: [];

	console.log(carouselImages);
	// add a new key-value pair field for newMember. Create a key
	// imageLocation and assign the value of the imagePath state as its
	// value
	const addImage = event => {
		event.preventDefault();
		if (imagePath !== "") {
			props.addCarouselImage({
				variables: {
					original: imagePath,
					thumbnail: imagePath
				},
				refetchQueries: [{ query: getCarouselImagesQuery }]
			});
			Swal.fire({
				icon: "success",
				title: "Successfully added a new image"
			});
			setImagePath("");
		} else {
			Swal.fire({
				icon: "warning",
				title: "Ooops, Please choose an image to upload!"
			});
		}
	};
	console.log(props);
	const deleteImage = event => {
		event.preventDefault();
		let deleteImageId = event.target.id;

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes"
		}).then(result => {
			if (result.value) {
				props.deleteCarouselImage({
					variables: { id: deleteImageId },
					refetchQueries: [{ query: getCarouselImagesQuery }]
				});
				Swal.fire("Deleted!", "Image has been deleted.", "success");
			}
		});
	};
	return (
		<div>
			<h1 className="text-center">Manage Carousel</h1>
			<div className="jumbotron">
				<div className="row">
					<div className="col-lg-3">
						<div className="card sticky">
							<div className="card-header text-center">
								<label className="label" htmlFor="image">
									Add Image
								</label>
							</div>
							<form className="form-inline">
								<div className="card-body">
									<input
										id="image"
										type="file"
										accept="image/png"
										className="form-control-file"
										ref={fileRef}
										onChange={imagePathHandler}
									/>
								</div>
							</form>
							<div className="card-footer">
								<button
									type="button"
									className="btn btn-outline-primary btn-lg btn-block"
									onClick={addImage}
								>
									Upload Image
								</button>
							</div>
						</div>
					</div>
					<div className="col-lg-9">
						<table className="table table-image table-responsive table-striped">
							<thead>
								<tr>
									<th>Image</th>
									<th></th>
									<th></th>
									<th></th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								{carouselImages.map(carousel => {
									return (
										<tr key={carousel.id}>
											<td className="w-25">
												<img
													src={
														nodeServer() +
														carousel.original
													}
													className="img-fluid img-thumbnail"
												/>
											</td>
											<td></td>
											<td></td>
											<td></td>
											<td className="mx-auto align-middle">
												<button
													id={carousel.id}
													type="button"
													onClick={deleteImage}
													className="btn btn-danger btn-block"
												>
													Delete
												</button>
											</td>
										</tr>
									);
								})}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default compose(
	graphql(addCarouselImageMutation, { name: "addCarouselImage" }),
	graphql(getCarouselImagesQuery, { name: "getCarouselImages" }),
	graphql(deleteCarouselImageMutation, { name: "deleteCarouselImage" })
)(ManageCarousel);
