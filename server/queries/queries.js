const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const uuid = require("uuid/v1");
const fs = require("fs");

// mongoose models
const mongoose = require("mongoose");
const Transaction = require("../models/Transaction");
const Unit = require("../models/Unit");
const User = require("../models/User");
const Carousel = require("../models/Carousel");

const moment = require("moment");
// CRUD
// type Query == Retrieve / Read a Data in a collection/table...
// type Mutation == Create/ Update/ Delete
// Bcrypt
const bcrypt = require("bcrypt");
// Resolver for Date Schema to be able to handle dateTime data types..
const customeScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`
	# this is a comment
	# type Query is the root of all GraphQL queries
	# this is used for "GET" request

	scalar Date

	type UserType {
		id: ID!
		username: String
		password: String
		email: String
		firstName: String
		lastName: String
		contact_number: String
		role: String
	}

	type UnitType {
		id: ID!
		name: String
		description: String
		room: String
		bed_size: String
		type: String
		image: String
	}

	type TransactionType {
		id: ID!
		userId: ID!
		check_in: Date
		check_out: Date
		status: String
		user: UserType
	}

	type CarouselType {
		id: ID!
		original: String
		thumbnail: String
	}

	type Query {
		getUsers: [UserType]
		getUser(id: ID!): UserType

		getUnits: [UnitType]
		getUnit(id: ID!): UnitType

		getTransactions: [TransactionType]
		getTransaction: TransactionType

		getCarousels: [CarouselType]
	}

	#Create Update Delete
	type Mutation {
		createUser(
			username: String
			password: String
			email: String
			firstName: String
			lastName: String
			contact_number: String
		): UserType

		updateUser(
			id: ID!
			password: String
			email: String
			firstName: String
			lastName: String
			contact_number: String
		): UserType

		deleteUser(id: ID!): Boolean

		loginUser(username: String!, password: String!): UserType

		createUnit(
			name: String
			description: String
			room: String
			bed_size: String
			type: String
			image: String
		): UnitType

		updateUnit(
			id: ID!
			name: String
			description: String
			room: String
			bed_size: String
			type: String
			image: String
		): UnitType

		deleteUnit(id: ID!): Boolean

		createTransaction(
			userId: String
			check_in: Date
			check_out: Date
		): TransactionType

		updateTransaction(id: ID!, status: String): Boolean

		deleteTransaction(id: ID!): Boolean

		addCarouselImage(original: String): CarouselType

		deleteCarouselImage(id: ID!): Boolean
	}
`;

const resolvers = {
	// run this if the query is executed
	Query: {
		getUsers: () => {
			return User.find({});
		},
		getUser: (_parent, args) => {
			return User.findById(args.id);
		},

		getUnits: () => {
			return Unit.find({});
		},
		getUnit: (_parent, args) => {
			return Unit.findById(args.id);
		},

		getTransactions: () => {
			return Transaction.find({});
		},
		getTransaction: (_parent, args) => {
			return Transaction.findById(args.id);
		},

		getCarousels: (_parent, args) => {
			return Carousel.find({});
		}
	},

	Mutation: {
		deleteCarouselImage: (_parent, args) => {
			return Carousel.findByIdAndDelete(args.id).then((carousel, err) => {
				console.log("err: ", !err);
				console.log("carousel: ", carousel);
				if (err || !carousel) {
					console.log("delete failed. carousel image not found");
					return false;
				}
				console.log("carousel deleted");
				return true;
			});
		},
		addCarouselImage: (_parent, args) => {
			// assign the variable imageString to the value of the encoded file
			let imageString = args.original;

			// in order to decode the encoded data, we need to remove the text
			// before the encoded string. we need to remove data:image/png;base64,
			let imageBase = imageString.split(";base64,").pop();

			// declare the location of the images folder in the server
			// and assign a unique name for our file using uuid
			let imageLocation = "images/" + uuid() + ".png";

			// syntax fs.writeFile(filename/descriptor, data, options, cb func)
			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let newCarouselImage = Carousel({
				original: imageLocation,
				thumbnail: imageLocation
			});

			return newCarouselImage.save();
		},

		createUser: (_parent, args) => {
			let newUser = User({
				username: args.username,
				password: bcrypt.hashSync(args.password, 10),
				email: args.email,
				firstName: args.firstName,
				lastName: args.lastName,
				contact_number: args.contact_number
			});

			return newUser.save();
		},

		updateUser: (_parent, args) => {
			if (args.password) {
				let updateUser = User.findByIdAndUpdate(args.id, {
					$set: {
						password: bcrypt.hashSync(args.password, 10),
						email: args.email,
						firstName: args.firstName,
						lastName: args.lastName,
						contact_number: args.contact_number
					}
				});
				return updateUser;
			} else {
				let updateUser = User.findByIdAndUpdate(args.id, {
					$set: {
						email: args.email,
						firstName: args.firstName,
						lastName: args.lastName,
						contact_number: args.contact_number
					}
				});
				return updateUser;
			}
		},
		deleteUser: (_parent, args) => {
			// let deleteUser = User.findByIdAndDelete(args.id);

			// return deleteUser;

			return User.findByIdAndDelete(args.id).then((user, err) => {
				console.log("err: ", !err);
				console.log("user: ", user);
				if (err || !user) {
					console.log("delete failed. no user found");
					return false;
				}
				console.log("user deleted");
				return true;
			});
		},
		loginUser: (_parent, args) => {
			return User.findOne({ username: args.username }).then(user => {
				if (user === null) {
					return null;
				}

				if (bcrypt.compareSync(args.password, user.password)) {
					return user;
				}
			});
		},

		createUnit: (_parent, args) => {
			let newUnit = Unit({
				name: args.name,
				description: args.description,
				room: args.room,
				bed_size: args.bed_size,
				type: args.type,
				image: args.image
			});

			return newUnit.save();
		},
		updateUnit: (_parent, args) => {
			let updateUnit = Unit.findByIdAndUpdate(args.id, {
				$set: {
					name: args.name,
					description: args.description,
					room: args.room,
					bed_size: args.bed_size,
					type: args.type,
					image: args.image
				}
			});
			return updateUnit;
		},
		deleteUnit: (_parent, args) => {
			return Unit.findByIdAndDelete(args.id).then((unit, err) => {
				if (err || !unit) {
					console.log("delete failed. no unit found");
					return false;
				}
				console.log("unit deleted");
				return true;
			});
		},

		createTransaction: (_parent, args) => {
			console.log(args);
			let newTransaction = Transaction({
				userId: args.userId,
				check_in: moment(args.check_in).add(1, "d"),
				check_out: moment(args.check_out).add(1, "d")
			});

			return newTransaction.save();
		},
		updateTransaction: (_parent, args) => {
			let updateTransaction = Transaction.findByIdAndUpdate(args.id, {
				$set: {
					status: args.status
				}
			});
			return updateTransaction.then((transaction, err) => {
				if (err || !transaction) {
					return false;
				}

				return true;
			});
		},
		deleteTransaction: (_parent, args) => {
			return Transaction.findByIdAndDelete(args.id).then(
				(transaction, err) => {
					if (err || !transaction) {
						console.log("delete failed. Transaction not found");
						return false;
					}
					console.log("Transaction deleted");
					return true;
				}
			);
		}
	},

	TransactionType: {
		user: (parent, args) => {
			return User.findById(parent.userId);
		}
	}
};

// create instance of ApolloSErver
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
