import React, { useState } from "react";
import { updateUserMutation } from "../queries/mutations";
import { getUserQuery } from "../queries/queries";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import Swal from "sweetalert2";

const showAlert = (title, text, icon) => {
	Swal.fire({
		title: title,
		text: text,
		icon: icon
	});
};

const User = props => {
	const [email, setEmail] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [contactNumber, setContactNumber] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPasword] = useState("");
	const [userId, setUserId] = useState("");

	let userData = props.getUser.getUser ? props.getUser.getUser : [];

	if (!props.getUser.loading) {
		const setUserInfo = () => {
			setEmail(userData.email);
			setFirstName(userData.firstName);
			setLastName(userData.lastName);
			setContactNumber(userData.contact_number);
			setUserId(userData.id);
		};

		if (userId === "") {
			setUserInfo();
		}
	}

	const inputHandler = event => {
		if (event.target.id === "email") {
			setEmail(event.target.value);
		} else if (event.target.id === "firstName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lastName") {
			setLastName(event.target.value);
		} else if (event.target.id === "contactNumber") {
			setContactNumber(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "confirmPassword") {
			setConfirmPasword(event.target.value);
		}
	};

	const updateUser = event => {
		event.preventDefault();

		if (
			password === confirmPassword &&
			password !== "" &&
			confirmPassword !== ""
		) {
			props.updateUser({
				variables: {
					id: props.match.params.id,
					email: email,
					firstName: firstName,
					lastName: lastName,
					contact_number: contactNumber,
					password: password
				}
			});
			showAlert("Success", "Profile Updated!", "success");
		} else {
			props.updateUser({
				variables: {
					id: props.match.params.id,
					email: email,
					firstName: firstName,
					lastName: lastName,
					contact_number: contactNumber
				}
			});
			showAlert("Success", "Profile Updated!", "success");
		}

		if (password !== confirmPassword) {
			showAlert("Ooops", "Password not match", "warning");
		} else {
			props.updateUser({
				variables: {
					id: props.match.params.id,
					email: email,
					firstName: firstName,
					lastName: lastName,
					contact_number: contactNumber
				}
			});
			showAlert("Success", "Profile Updated!", "success");
		}
	};

	return (
		<div>
			<div className="container">
				<div className="jumbotron">
					<h1 className="text-center"> Profile </h1>
					<div className="row">
						<div className="col-lg-6 mx-auto">
							<form onSubmit={updateUser}>
								<div className="form-group">
									<label htmlFor="email">
										Email address:
									</label>
									<input
										type="email"
										className="form-control"
										id="email"
										onChange={inputHandler}
										value={email}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="firstName">
										First Name
									</label>
									<input
										type="text"
										className="form-control"
										id="firstName"
										onChange={inputHandler}
										value={firstName}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="lasName">Last Name</label>
									<input
										type="text"
										className="form-control"
										id="lastName"
										onChange={inputHandler}
										value={lastName}
									/>
								</div>
								<div className="form-group">
									<label htmlFor="contactNumber">
										Contact Number
									</label>
									<input
										type="text"
										className="form-control"
										id="contactNumber"
										onChange={inputHandler}
										value={contactNumber}
									/>
								</div>
								<hr />
								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input
										type="password"
										className="form-control"
										placeholder="Enter New Password"
										id="password"
										onChange={inputHandler}
										value={password}
									/>
									<label htmlFor="password">
										Confirm Password
									</label>
									<input
										type="password"
										className="form-control"
										placeholder="Confirm New Password"
										id="confirmPassword"
										onChange={inputHandler}
										value={confirmPassword}
									/>
								</div>
								<button
									type="submit"
									className="btn btn-primary btn-block"
								>
									Update
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default compose(
	graphql(updateUserMutation, { name: "updateUser" }),
	graphql(getUserQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getUser"
	})
)(User);
