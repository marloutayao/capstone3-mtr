import React from "react";
import ImageGallery from "react-image-gallery";
import { graphql } from "react-apollo";
import { getCarouselImagesQuery } from "../queries/queries";
import { nodeServer } from "../function.js";

const Home = props => {
	const carouselImages = props.getCarouselImages.getCarousels
		? props.getCarouselImages.getCarousels
		: [];

	const carouselImg = [];
	carouselImages.map(images => {
		carouselImg.push({
			original: nodeServer() + images.original,
			thumbnail: nodeServer() + images.thumbnail
		});
	});

	return (
		<div>
			<ImageGallery
				items={carouselImg}
				infinite={true}
				showPlayButton={false}
				showBullets={true}
				autoPlay={true}
				showThumbnails={true}
				sizes="{
					max-height:500
				}"
			/>
			<hr />
			<div className="container">
				<div className="row">
					<div className="col-md-12 col-lg-12 col-12 text-center">
						<h2>
							<p>EXPERIENCE A GOOD QUALITY,</p>
							<p>ENJOY FANTASTIC OFFERS</p>
						</h2>
						<h6> SPECIAL OCCASIONS</h6>
					</div>

					<div className="col-md-6 col-12 col-lg-4">
						<div className="card">
							<div className="card-body">
								<h4 className="card-title text-center">
									Birthday
								</h4>
								<img
									className="card-img-top"
									src="./assets/images/home/birthday.jpg"
									alt="Birthday"
								/>
							</div>
						</div>
					</div>
					<div className="col-md-6 col-12 col-lg-4">
						<div className="card">
							<div className="card-body">
								<h4 className="card-title text-center">
									Wedding
								</h4>
								<img
									className="card-img-top"
									src="./assets/images/home/wedding.jpg"
									alt="Wedding"
								/>
							</div>
						</div>
					</div>
					<div className="col-md-6 col-12 col-lg-4">
						<div className="card">
							<div className="card-body">
								<h4 className="card-title text-center">
									Debut
								</h4>
								<img
									className="card-img-top"
									src="./assets/images/home/debut.jpg"
									alt="Debut"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default graphql(getCarouselImagesQuery, { name: "getCarouselImages" })(
	Home
);
