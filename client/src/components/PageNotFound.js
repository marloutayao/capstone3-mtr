import React from "react";
const PageNotFound = props => {
	return (
		<div>
			<div className="container">
				<div className="jumbotron text-center mx-auto mt-5">
					<h1> Oooops, Page Not Found </h1>

					<img
						src="../assets/images/404/404.gif"
						className="mx-auto d-block img-fluid"
					/>
				</div>
			</div>
		</div>
	);
};

export default PageNotFound;
