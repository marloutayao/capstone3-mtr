import { gql } from "apollo-boost";

const getReservationsQuery = gql`
	{
		getTransactions {
			id
			check_in
			check_out
			status
			userId
			user {
				username

				email
				firstName
				lastName
				contact_number
				role
			}
		}
	}
`;

const getUserQuery = gql`
	query($id: ID!) {
		getUser(id: $id) {
			id
			username
			password
			email
			firstName
			lastName
			contact_number
		}
	}
`;

const getCarouselImagesQuery = gql`
	{
		getCarousels {
			id
			original
			thumbnail
		}
	}
`;

export { getReservationsQuery, getUserQuery, getCarouselImagesQuery };
