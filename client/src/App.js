import React, { useState } from "react";
import "./App.css";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import NavBar from "./components/NavBar";
import User from "./components/User";
import Transaction from "./components/Transaction";
import Home from "./components/Home";
import Login from "./components/Login";
import Register from "./components/Register";
import Reservation from "./components/Reservation";
import UserReservation from "./components/UserReservation";
import PageNotFound from "./components/PageNotFound";
import ManageCarousel from "./components/ManageCarousel";

const client = new ApolloClient({ uri: "http://localhost:4000/MTReservation" });

function App() {
	const [username, setUsername] = useState(localStorage.getItem("username"));
	const [role, setRole] = useState(localStorage.getItem("role"));
	const [userId, setUserId] = useState(localStorage.getItem("userId"));

	const updateSession = () => {
		setUsername(localStorage.getItem("username"));
		setRole(localStorage.getItem("role"));
		setUserId(localStorage.getItem("userId"));
	};
	const Logout = () => {
		localStorage.clear();
		setUsername("");
		setRole("");
		setUserId("");
		return <Redirect to="/login" />;
	};

	const loggedUser = props => {
		return <Login {...props} updateSession={updateSession} />;
	};

	return (
		<ApolloProvider client={client}>
			<BrowserRouter>
				<NavBar username={username} />
				<Switch>
					<Route exact path="/" component={Home} />

					<Route exact path="/login" render={loggedUser} />
					<Route exact path="/register" component={Register} />

					<Route exact path="/logout" component={Logout} />

					{role === "admin" ? (
						<Switch>
							<Route
								exact
								path="/transactions"
								component={Transaction}
							/>
							<Route exact path="/users/:id" component={User} />
							<Route
								exact
								path="/reservations/:id"
								component={PageNotFound}
							/>
							<Route
								exact
								path="/reservations"
								component={PageNotFound}
							/>
							<Route
								exact
								path="/carousel"
								component={ManageCarousel}
							/>
						</Switch>
					) : (
						""
					)}

					{role === "user" ? (
						<Switch>
							<Route
								exact
								path="/reservations/:id"
								component={UserReservation}
							/>
							<Route exact path="/users/:id" component={User} />
							<Route
								exact
								path="/transactions"
								component={PageNotFound}
							/>
							<Route
								exact
								path="/reservations"
								component={Reservation}
							/>
							<Route
								exact
								path="/carousel"
								component={PageNotFound}
							/>
						</Switch>
					) : (
						""
					)}

					<Route exact path="/reservations" component={Reservation} />
					<Route exact path="/carousel" component={PageNotFound} />
					<Route
						exact
						path="/transactions"
						component={PageNotFound}
					/>
					<Route exact path="/users/:id" component={PageNotFound} />
					<Route
						exact
						path="/reservations/:id"
						component={PageNotFound}
					/>
				</Switch>
			</BrowserRouter>
		</ApolloProvider>
	);
}

export default App;
