import React, { useReducer } from "react";
import { DateRangeInput } from "@datepicker-react/styled";
import moment from "moment";
import { ThemeProvider } from "styled-components";
import { createReservationMutation } from "../queries/mutations.js";
import {
	getReservationsQuery,
	getCarouselImagesQuery
} from "../queries/queries.js";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import ImageGallery from "react-image-gallery";
import Swal from "sweetalert2";

import { nodeServer } from "../function.js";

const initialState = {
	startDate: null,
	endDate: null,
	focusedInput: null
};

function reducer(state, action) {
	switch (action.type) {
		case "focusChange":
			return { ...state, focusedInput: action.payload };
		case "dateChange":
			return action.payload;
		default:
			throw new Error();
	}
}

const Reservation = props => {
	const carouselImages = props.getCarouselImages.getCarousels
		? props.getCarouselImages.getCarousels
		: [];

	const carouselImg = [];
	carouselImages.map(images => {
		carouselImg.push({
			original: nodeServer() + images.original,
			thumbnail: nodeServer() + images.thumbnail
		});
	});

	const [state, dispatch] = useReducer(reducer, initialState);
	let getReservationsData = props.getReservations.loading
		? []
		: props.getReservations.getTransactions;

	const reserveEvent = event => {
		event.preventDefault();
		if (localStorage.getItem("userId")) {
			if (state.startDate === null || state.startDate === "") {
				Swal.fire(
					"Ooops,",
					"Please complete the input fields",
					"warning"
				);
			} else if (state.endDate === null || state.endDate === "") {
				Swal.fire(
					"Ooops,",
					"Please complete the input fields",
					"warning"
				);
			} else {
				props
					.createReservation({
						variables: {
							check_in: state.startDate,
							check_out: state.endDate,
							userId: localStorage.getItem("userId")
						},
						refetchQueries: [
							{
								query: getReservationsQuery
							}
						]
					})
					.then(res => {
						Swal.fire(
							"Success,",
							"Please wait for admin aproval!",
							"success"
						);
					});
			}
		} else {
			Swal.fire(
				"Ooops,",
				"Please log in first to book a reservation, Thank you!",
				"warning"
			);
		}
	};

	let disableStartDates = { date: [] };
	let disableEndDates = { date: [] };
	getReservationsData.map(res => {
		if (
			res.status !== "settled" &&
			res.status !== "pending" &&
			res.status !== "canceled"
		) {
			let timeStart = moment(res.check_in).subtract(1, "d");
			let check_in = timeStart.format("MM/DD/YYYY");
			let timeEnd = moment(res.check_out).subtract(1, "d");
			let check_out = timeEnd.format("MM/DD/YYYY");
			disableStartDates.date.push(check_in);
			disableEndDates.date.push(check_out);
		}
	});

	return (
		<div>
			<div className="jumbotron">
				<h1 className="text-center"> Reserve now! </h1>
			</div>
			<div className="container">
				<form onSubmit={reserveEvent}>
					<div className="row">
						<div className="col-lg-6 my-auto">
							<ThemeProvider
								theme={{
									breakpoints: ["32em", "48em", "64em"],
									reactDatepicker: {
										daySize: [36, 40],
										fontFamily: "system-ui, -apple-system",
										colors: {
											accessibility: "#D80249",
											selectedDay: "#d77fa1",
											selectedDayHover: "#e6b2c6",
											primaryColor: "#d8366f",
											white: "#d6e5fa"
										}
									}
								}}
							>
								<DateRangeInput
									vertical={true}
									onDatesChange={data =>
										dispatch({
											type: "dateChange",
											payload: data
										})
									}
									onFocusChange={focusedInput =>
										dispatch({
											type: "focusChange",
											payload: focusedInput
										})
									}
									startDate={state.startDate}
									endDate={state.endDate}
									focusedInput={state.focusedInput}
									minBookingDate={new Date()}
									isDateBlocked={date => {
										let getDates = moment(date);
										let formatedDates = getDates.format(
											"MM/DD/YYYY"
										);

										for (
											let x = 0;
											x < disableStartDates.date.length;
											x++
										) {
											for (
												let y = 0;
												y < disableEndDates.date.length;
												y++
											) {
												if (
													moment(
														formatedDates
													).isBetween(
														disableStartDates.date[
															x
														],
														disableEndDates.date[x],
														null,
														"[]"
													)
												) {
													return true;
												}
											}
										}
									}}
								/>
							</ThemeProvider>
							<hr />
							<button className="btn btn-outline-primary btn-block">
								{" "}
								Book now!
							</button>
						</div>

						<div className="col-lg-6">
							<ImageGallery
								items={carouselImg}
								infinite={true}
								showPlayButton={false}
								showBullets={true}
								autoPlay={true}
								showThumbnails={true}
								sizes="{
					max-height:500
				}"
							/>
						</div>
					</div>
				</form>
			</div>
		</div>
	);
};

export default compose(
	graphql(createReservationMutation, {
		name: "createReservation"
	}),
	graphql(getReservationsQuery, { name: "getReservations" }),
	graphql(getCarouselImagesQuery, { name: "getCarouselImages" })
)(Reservation);
