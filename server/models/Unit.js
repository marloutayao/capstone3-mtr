const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const unitSchema = new Schema(
	{
		name: { type: String },
		description: { type: String },
		room: { type: String },
		bed_size: { type: String },
		type: { type: String },
		image: { type: String }
	},

	{
		timestamps: true
	}
);

module.exports = mongoose.model("Unit", unitSchema);
