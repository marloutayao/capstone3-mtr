// Declare dependecies
const express = require("express");
const app = express();
const mongoose = require("mongoose");

// analyze the incoming request objects
const bodyParser = require("body-parser");

// Database connection

// Local
// mongoose.connect("mongodb://localhost:27017/mongoCubes", {
// 	useNewUrlParser:true
// });

// Online
mongoose.connect(
	"mongodb+srv://dbTest:test1234@mongo-train-r0efh.mongodb.net/MTReservation?retryWrites=true&w=majority",
	{
		useCreateIndex: true,
		useNewUrlParser: true
	}
);

// chech if connection succeeds
mongoose.connection.once("open", () => {
	console.log("Connected to online MongoDB server !!!");
});

// increase the limit of uploaded files
app.use(bodyParser.json({ limit: "15mb" }));

// allow users to access a folder in the server by serving the static data
// syntax: app.use("/path", express.static("folder to serve"))
app.use("/images", express.static("images"));

// import the instantiation ApolloServer
const server = require("./queries/queries");

// this -> app will served by ApolloServer
server.applyMiddleware({
	app,
	path: "/MTReservation"
});

// server initialization
app.listen(4000, () => {
	console.log(`🚀  Server ready at ${server.graphqlPath}`);
});
